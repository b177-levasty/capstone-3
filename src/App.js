import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import { Fragment, useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom'
import AppNavbar from './components/AppNavbar';
import Products from './pages/Products';
import Error from './pages/Error';
import Home from './pages/Home';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Login from './pages/Login';
import ProductView from './components/ProductView';
import Accounts from './pages/Accounts';
import CreateProduct from './pages/CreateProduct'
import MyOrders from './pages/MyOrders';
import AdminProduct from './pages/AdminProduct';
import ViewOrders from './pages/ViewOrders';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user]) 

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
          <Router>
              <AppNavbar />
              <Container>
              <div>
                <Routes>
                  <Route path="/" element={<Home />} />
                  <Route path="/products" element={<Products />} />
                  <Route path="/users" element={<Accounts />} />
                  <Route path="/products/:productId" element={<ProductView />} />
                  <Route path="/myorders" element={<MyOrders />} />
                  <Route path="/vieworders" element={<ViewOrders />} />
                  <Route path="/adminproducts" element={<AdminProduct />} />
                  <Route path="/products/create" element={<CreateProduct />} />
                  <Route path="/register" element={<Register />} />
                  <Route path="/login" element={<Login />} />            
                  <Route path="/logout" element={<Logout />} />

                  <Route path="*" element={<Error />} />
                </Routes>
              </div>
              </Container>
          </Router>
        </UserProvider>
        
      );
}

export default App;
