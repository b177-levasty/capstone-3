// import { Fragment } from 'react';
// import Banner from '../components/Banner';
// import { Route, Routes } from 'react-router-dom';
// import { BrowserRouter as Router } from 'react-router-dom';

// export default function Home(){
// 	return (
// 		<Fragment>
// 			<Banner />
// 		</Fragment>
// 		)
	
// }

import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import styles from '../App.css'
import { Fragment } from 'react';
import { Container, Form } from 'react-bootstrap';
import Banner from '../components/Banner';

export default function Home(){

	const { user, setUser } = useContext(UserContext);

	const data = {
	    title: "PH Comparts",
	    content: "Build and upgrade you desktop",
	    destination: "/products",
	    label: "Buy Now!"
	}

	return (
		(user.isAdmin !== true) ?
		<div className="position-absolute top-50 start-50 translate-middle text-center w-50 h-50 panel panel-default">
		
		<Form className="">
			<img
  			    className="mx-4 w-25 h-25 p-2"
  			    margin="auto"
  			    src="../Images/gaming-mouse.png"
  			/>
			<Banner data={data}/>
		</Form>
		</div>


		:
		<Container className="App">
			<h1>Welcome Admin</h1>
			<img
  			    className="mx-4 w-25 h-25 p-2"
  			    margin="auto"
  			    src="../Images/gaming-mouse.png"
  			/>
		</Container>
	)	
}
