import { Fragment, useEffect, useState, useContext } from 'react';
import ProductCard, { ProductCardTable } from '../components/ProductCard';
import UserContext from '../UserContext';
import { useParams, useNavigate, Link } from 'react-router-dom';
import { Card, Container, Row, Col, Form } from 'react-bootstrap'
import Table from 'react-bootstrap/Table'
import CreateProduct from './CreateProduct'

export default function Products() {

	const { user } = useContext(UserContext);

	const [products, setProducts] = useState([]);


	useEffect(() => {
		fetch('https://protected-fjord-90200.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(user.isAdmin == true) {
				setProducts(data)
			}
			else{
				setProducts(data.map(product => {
							return (
								<ProductCard key={product._id} productProp={product} />
							);
						}))
			}

			

		})
	}, []);

	return(	
		(user.isAdmin !== true) ?
	  	<Row className="mb-3 justify-content-center">

		   {products}

		</Row>
		:
		<Container className="btn justify-content-center">
		<br/>
		<Form>
		<Link className="btn btn-danger justify-content-center" to={`/products/create`}>Register Product</Link>
		</Form>
		<br/>
		<Fragment>
		    <ProductCardTable>
		       {products.map(product => <ProductCard key={product._id} productProp={product} />)}
		    </ProductCardTable>
		</Fragment>
        </Container>


	)
}