import { useState, useEffect, useContext } from 'react';
import { Navigate, useParams, useNavigate, Link } from 'react-router-dom'
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Home from './Home';

	
	export default function CreateProduct(){

		const history = useNavigate();

		const { user, setUser } = useContext(UserContext);

		const [name, setName] = useState('');
		const [description, setDescription] = useState('');
		const [price, setPrice] = useState('');
		// const [email, setEmail] = useState('');
		// const [password1, setPassword1] = useState('');
		// const [password2, setPassword2] = useState('');
		// const [isActive, setIsActive] = useState(false);


		console.log(name);
		console.log(description);
		console.log(price);	

		function createProduct(e){
			e.preventDefault();

			fetch('https://protected-fjord-90200.herokuapp.com/products/create', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                	name: name,
                	description: description,
                	price: price
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                if(data){
                	Swal.fire({
                		title: "Successfull",
                		icon: "success",
                		text: "Product successfully regitered"
                	})
                	history("/products");
                }
                else{
                	Swal.fire({
                		title: "Something went wrong",
                		icon: "error",
                		text: "Please try again."
                	})
                }
			   
			})

			
		}

		// useEffect(() => {
			
		// 	if((firstName !== '' && lastName !== '' && mobileNo !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
		// 		setIsActive(true);
		// 	}
		// 	else{
		// 		setIsActive(false);
		// 	}
		// }, [firstName, lastName, mobileNo, email, password1, password2]);


	return(
		(user.isAdmin !== true) ?
		    <Navigate to="/" />
		:
		<Form className="position-absolute top-50 start-50 translate-middle bi bi-caret-down-fill text-center" onSubmit={(e) => createProduct(e)}>
		  <h1>Register Product</h1>
		  <Form.Group className="mb-3" >

		  	<Form.Label>Product Name:</Form.Label>
		  	<Form.Control
		  		className="text-center"
		  		type="text" 
		  		placeholder="Enter product name" 
		  		value={name}
		  		onChange={e => setName(e.target.value)}
		  		required
		  	/>
		    <Form.Label>Product Description:</Form.Label>
		    <Form.Control 
		    	className="text-center"
		    	type="text" 
		    	placeholder="Enter product description" 
		    	value={description}
		    	onChange={e => setDescription(e.target.value)}
		    	required
		    />
		    <Form.Label>Product Price:</Form.Label>
		    <Form.Control 
		    	className="text-center"
		    	type="number" 
		    	placeholder="Enter products price" 
		    	value={price}
		    	onChange={e => setPrice(e.target.value)}
		    	required
		    />
		    </Form.Group>
		    <Button variant="primary" type="submit" id="submitBtn">
		  	  Submit
		  	</Button>
		</Form>
	)
}
