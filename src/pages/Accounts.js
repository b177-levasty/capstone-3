import { Fragment, useEffect, useState, useContext } from 'react';
import AccountCard, { AccountCardTable } from '../components/AccountCard';
import UserContext from '../UserContext';
import { useParams, useNavigate, Link } from 'react-router-dom';
import {Container, Row, Col, Form } from 'react-bootstrap'
import Table from 'react-bootstrap/Table'
import CreateProduct from './CreateProduct'

export default function Account() {

	const { user } = useContext(UserContext);

	const [users, setUsers] = useState([]);

	useEffect(() => {
		fetch('https://protected-fjord-90200.herokuapp.com/users/all', {
			method: "POST"
		}

			)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUsers(data)

		})
	}, []);

	return(
	    <Fragment>
	        <AccountCardTable>
	           {users.map(user => <AccountCard key={user._id} usersProp={user} />)}
	        </AccountCardTable>
	    </Fragment>
	)

}