import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Login() {

        const { user, setUser } = useContext(UserContext);

        const [email, setEmail] = useState('');
        const [password, setPassword] = useState('');
        const [isActive, setIsActive] = useState(true);

        function authenticate(e) {

            e.preventDefault();

            fetch('https://protected-fjord-90200.herokuapp.com/users/login',{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                if(typeof data.access !== "undefined"){
                    localStorage.setItem('token', data.access);
                    retrieveUserDetails(data.access);
                    Swal.fire({
                        title: "Login Successful",
                        icon: "success",
                        text: "Welcome!"
                    })
                }
                else{
                    Swal.fire({
                        title: "Authentication failed",
                        icon: "error",
                        text: "Check your login deatils and try again."
                    })
                }
            })

            setEmail('');
            setPassword('');

        }

        const retrieveUserDetails = (token) => {
            fetch('https://protected-fjord-90200.herokuapp.com/users/details',{
                headers: {
                    Authorization: `Bearer ${ token }`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
        }

        useEffect(() => {

            if(email !== '' && password !== ''){
                setIsActive(true);
            }else{
                setIsActive(false);
            }

        }, [email, password]);


    return (
        (user.id !== null) ?
            <Navigate to="/" />
        :
        <Container>
        <div className="position-absolute top-50 start-50 translate-middle text-center w-50 h-50 panel panel-default"  onSubmit={(e) => authenticate(e)}>
        <h1>Login</h1>
        <Form className="panel-body">
              <Form.Group 
                className="mb-3 " 
                controlId="userEmail"
              >
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    className="text-center"
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                  We'll never share your email with anyone else.
                </Form.Text>
              </Form.Group>

              <Form.Group 
                className="mb-3" 
                controlId="password"
              >
                <Form.Label>Password</Form.Label>
                <Form.Control
                    className="text-center"
                    type="password" 
                    placeholder="Enter Password" 
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
              </Form.Group>
              { isActive ? 
                <Button variant="danger" type="submit" id="submitBtn">
                    Submit
                </Button>
                : 
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
            </Form>
            </div>
            </Container>
    )
}
