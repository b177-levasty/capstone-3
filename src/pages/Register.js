import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Login from './Login';
import React from 'react'

	
	export default function Register(){

		const { user, setUser } = useContext(UserContext);
		const [firstName, setFirstName] = useState('');
		const [lastName, setLastName] = useState('');
		const [mobileNo, setMobileNo] = useState('');
		const [email, setEmail] = useState('');
		const [password1, setPassword1] = useState('');
		const [password2, setPassword2] = useState('');
		const [isActive, setIsActive] = useState(false);

		const history = useNavigate();

		console.log(email);
		console.log(password1);
		console.log(password2);	

		// function registerUser(e){
		// 	e.preventDefault();

		// 	fetch('http://localhost:4000/users/register', {
  //               method: 'POST',
  //               headers: {
  //                   'Content-Type': 'application/json'
  //               },
  //               body: JSON.stringify({
  //               	firstName: firstName,
  //               	lastName: lastName,
  //                   email: email,
  //                   mobileNo: mobileNo,
  //                   password: password1
  //               })
  //           })
  //           .then(res => res.json())
  //           .then(data => {
  //               console.log(data)

            
		// 		if(data === true){
		// 		Swal.fire({
  //                       title: "Register Successful",
  //                       icon: "success",
  //                       text: "Welcome to Zuitt!"
  //                   })
		// 		setFirstName('');
		// 		setLastName('');
		// 		setEmail('');
		// 		setMobileNo('');
		// 		setPassword1('');
		// 		setPassword2('');
		// 	}
		// 	else{
		// 		Swal.fire({
		// 		    title: "Register failed",
		// 		    icon: "error",
		// 		    text: "Try again."
		// 		})
		// 	}
			   
		// 	})

			
		// }

		function registerUser(e){
				e.preventDefault();

				fetch('https://protected-fjord-90200.herokuapp.com/users/checkEmail', {
				    method: "POST",
				    headers: {
				        'Content-Type': 'application/json'
				    },
				    body: JSON.stringify({
				        email: email
				    })
				})
				.then(res => res.json())
				.then(data => {

				    console.log(data);

				    	if(data === true){
							Swal.fire({
				    			title: 'Error',
				    			icon: 'error',
				    			text: 'Email is already used.'	
				    		});
				    	}else {

				                fetch('https://protected-fjord-90200.herokuapp.com/users/register', {
				                    method: "POST",
				                    headers: {
				                        'Content-Type': 'application/json'
				                    },
				                    body: JSON.stringify({
				                        firstName: firstName,
				                        lastName: lastName,
				                        mobileNo: mobileNo,
				                        email: email,
				                        password: password1
				                    })
				                })
				                .then(res => res.json())
				                .then(data => {

				                    console.log(data);

				                    if (data === true) {

				                        // Clear input fields
				                        setFirstName('');
				                        setLastName('');
				                        setEmail('');
				                        setMobileNo('');
				                        setPassword1('');
				                        setPassword2('');

				                        Swal.fire({
				                            title: 'Registration successful',
				                            icon: 'success',
				                            text: 'Welcome to PH Comparts!'
				                        });

				                        history("/login");

				                    } else {

				                        Swal.fire({
				                            title: 'Something wrong',
				                            icon: 'error',
				                            text: 'Please try again.'   
				                        });

				                    }
				                })
				            };

				})
				// Clear input fields
				setEmail('');
				setPassword1('');
				setPassword2('');

				//alert('Thank you for registering!');
			}


		useEffect(() => {
			
			if((firstName !== '' && lastName !== '' && mobileNo !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
				setIsActive(true);
			}
			else{
				setIsActive(false);
			}
		}, [firstName, lastName, mobileNo, email, password1, password2]);


	return(
		(user.id !== null) ?
		    <Navigate to="/login" />
		:
		<Form className="position-absolute top-50 start-50 translate-middle bi bi-caret-down-fill text-center" onSubmit={(e) => registerUser(e)}>
		  <h1>Register</h1>
		  <Form.Group className="mb-3" controlId="userEmail">

		  	<Form.Label>First Name:</Form.Label>
		  	<Form.Control
		  		className="text-center"
		  		type="text" 
		  		placeholder="Enter first name" 
		  		value={firstName}
		  		onChange={e => setFirstName(e.target.value)}
		  		required
		  	/>
		    <Form.Label>Last Name:</Form.Label>
		    <Form.Control 
		    	className="text-center"
		    	type="text" 
		    	placeholder="Enter email" 
		    	value={lastName}
		    	onChange={e => setLastName(e.target.value)}
		    	required
		    />
		    <Form.Label>Email Address:</Form.Label>
		    <Form.Control 
		    	className="text-center"
		    	type="email" 
		    	placeholder="Enter email" 
		    	value={email}
		    	onChange={e => setEmail(e.target.value)}
		    	required
		    />
		    <Form.Label>Mobile Number:</Form.Label>
		    <Form.Control 
		    	className="text-center"
		    	type="tel"
		    	maxLength="11" 
		    	placeholder="Enter mobile" 
		    	value={mobileNo}
		    	onChange={e => setMobileNo(e.target.value)}
		    	required
		    />
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  </Form.Group>
		{/*Password1*/}
		  <Form.Group className="mb-3" controlId="password1">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    	className="text-center"
		    	type="password" 
		    	placeholder="Password"
		    	value={password1}
		    	onChange={e => setPassword1(e.target.value)}
		    	required 
		    />
		  </Form.Group>
		{/*Password2*/}
		  <Form.Group className="mb-3" controlId="password2">
		    <Form.Label>Verify Password</Form.Label>
		    <Form.Control 
		    	className="text-center"
		    	type="password" 
		    	placeholder="Verify Password"
		    	value={password2}
		    	onChange={e => setPassword2(e.target.value)}
		    	required 
		    />
		  </Form.Group>

		  { isActive ?
		  	<Button variant="danger" type="submit" id="submitBtn">
		  	  Submit
		  	</Button>
		  	:
		  	<Button variant="danger" type="submit" id="submitBtn" disabled>
		  	  Submit
		  	</Button>
		  }	  
		</Form>
	)
}
