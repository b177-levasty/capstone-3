// import Banner from '../components/Banner';
// import { Route, Routes } from 'react-router-dom'
// import { Fragment } from 'react';

// export default function Error() {
//    function preventDef(e){
//     e.preventDefault()

//    }
//    return (
//         <div>
//             <h1>Page Not Found</h1>
//             <h3>Go back to <a href="/">Homepage</a></h3>
//         </div>
//     ) 
   
// }

import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }
    
    return (
        <Banner data={data}/>
    )
}


