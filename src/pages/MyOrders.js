import { Fragment, useEffect, useState, useContext } from 'react';
import OrderCards, { OrderCardTable } from '../components/OrderCards'
import React from 'react';
import UserContext from '../UserContext'
import Swal from 'sweetalert2';
import {Container, Row, Col, Form } from 'react-bootstrap'
import { useParams, useNavigate, Link } from 'react-router-dom';
import Table from 'react-bootstrap/Table'

export default function MyOrders() {

const { user, setUser } = useContext(UserContext);

const [orders, setOrders] = useState([]);

const fetchDataOrders = () => {
    fetch('https://protected-fjord-90200.herokuapp.com/users/myOrders', {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);

        setOrders(data.usersOrder)
    })
}

useEffect(() => {
    fetchDataOrders()
}, []);

return(
    <Fragment>
        <OrderCardTable>
           {orders.map(order => <OrderCards key={order._id} orderProp={order} />)}
        </OrderCardTable>
    </Fragment>
)
}

