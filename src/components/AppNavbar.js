import React, { useState, useContext } from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import Stack from 'react-bootstrap/Stack';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';

export default function AppNavbar(){

	const { user } = useContext(UserContext);

	return (
		(user.isAdmin !== true) ?
		<Navbar bg="dark" expand="lg" variant="dark">
		  <Container>
		    <Navbar.Brand as={Link} to="/" >Comparts</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="me-auto">
		        <Nav.Link as={Link} to="/" exact>Home</Nav.Link>
		        <Nav.Link as={Link} to="/products" exact>Products</Nav.Link>
		        {/*<Nav.Link as={Link} to="/myorders" exact>My Orders</Nav.Link>*/}
		        {(user.id !== null) ?
		        	// <Nav.Link  as={Link} to="/logout" exact>Logout</Nav.Link>
		        	<React.Fragment>
		        		<Nav.Link as={Link} to="/myorders" exact>My Orders</Nav.Link>
		        		<Nav.Link as={Link} to="/logout" exact>Logout</Nav.Link>
		        	</React.Fragment>
		        	:
		        	<React.Fragment>
		        		<Nav.Link as={Link} to="/login" exact>Login</Nav.Link>
		        		<Nav.Link as={Link} to="/register" exact>Register</Nav.Link>
		        	</React.Fragment>
		        }
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
		:
		<Navbar bg="dark" expand="lg" variant="dark">
		  <Container>
		    <Navbar.Brand as={Link} to="/">Comparts</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="me-auto">
		        <Nav.Link as={Link} to="/adminproducts" exact>Product List</Nav.Link>
		        <Nav.Link as={Link} to="/users" exact>Account Details</Nav.Link>
		        <Nav.Link as={Link} to="/vieworders" exact>View Orders</Nav.Link>
		        <Nav.Link as={Link} to="/logout" exact>Logout</Nav.Link>
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>        
	)
}