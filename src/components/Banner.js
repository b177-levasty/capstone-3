// import {Carousel, Row, Col} from 'react-bootstrap';
// import Image from 'react-bootstrap/Image';
// import Card from 'react-bootstrap/Card';
// import { Container } from 'react-bootstrap';
// //import mobo from '../Images/motherboard.png';

// function BannerCarousel() {
//   return (
//   	<Row className="justify-content-center">
//   		<Col xs={12} md={9} className="w-50 h-50">
//   			    <>
//   			      <Card className="m-5">
//   			        <Card.Img variant="top" src="../Images/logo.png" className="rounded w-10" />
//   			        <Card.Body>
// {/*  			          <Card.Text>
//   			            Some quick example text to build on the card title and make up the
//   			            bulk of the card's content.
//   			          </Card.Text>*/}
//   			        </Card.Body>
//   			      </Card>
//   			    </>

//   			  {/*<br />*/}
  			
//   			  <h3 className="w-50 h-50">Sample Products</h3>

//   			  {/*<br />*/}
 
//   			  <Carousel  fade>
//   			    <Carousel.Item>
//   			      <img
//   			        className="mx-4 w-20 h-20"
//   			        margin="auto"
//   			        src="../Images/motherboard.png"
//   			        // alt="First slide"
//   			      />
//   			      {/*<Carousel.Caption>
//   			        <h3>First slide label</h3>
//   			        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
//   			      </Carousel.Caption>*/}
//   			    </Carousel.Item>
//   			    <Carousel.Item>
//   			      <img
//   			        className="mx-4 w-20 h-20"
//   			        src="../Images/keyboard.jpg"
//   			        // alt="Second slide"
//   			      />

//   			      {/*<Carousel.Caption>
//   			        <h3>Second slide label</h3>
//   			        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
//   			      </Carousel.Caption>*/}
//   			    </Carousel.Item>
//   			    <Carousel.Item>
//   			      <img
//   			        className="mx-4 w-20 h-20"
//   			        src="../Images/mouse.jpg"
//   			        // alt="Third slide"
//   			      />

//   			      {/*<Carousel.Caption>
//   			        <h3>Third slide label</h3>
//   			        <p>
//   			          Praesent commodo cursus magna, vel scelerisque nisl consectetur.
//   			        </p>
//   			      </Carousel.Caption>*/}
//   			    </Carousel.Item>
//   			  </Carousel>
//   		</Col>
//   	</Row>
//   	); 
// }

// export default BannerCarousel;

import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}){
  console.log(data);
  const {title, content, destination, label} = data;

  return (
    <Row>
      <Col className="p-5">
        <h1>{title}</h1>
        <p>{content}</p>
        <Link to={destination}>{label}</Link>
      </Col>
    </Row>
  )
}


