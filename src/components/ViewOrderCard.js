import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import UserContext from '../UserContext';
import Table from 'react-bootstrap/Table'
import React, { useState, useContext } from 'react';


export function ViewOrderCardTable({ children }) {

const { user } = useContext(UserContext);

return (
    (user.isAdmin !== true) ?

        // <Card>
        //     {children}
        // </Card>

        // <Row>
        //   <Col className="p-5">
        //     <h1>{title}</h1>
        //     <p>{content}</p>
        //     <Link to={destination}>{label}</Link>
        //   </Col>
        // </Row>
        <div>
            <h1>Page Not Found</h1>
            <h3>Go back to <a href="/">Homepage</a></h3>
        </div>

    :
        <Container>
           <Table striped bordered hover variant="dark">
              <thead>
                <tr>
                </tr>
              </thead>
              <thead>
                <tr>
                  <th className="w-25">Purchase Date</th>
                  <th className="w-25">Email</th>
                  <th className="w-25">User ID</th>
                  {/*<th className="w-25">Order ID</th>*/}
                  {/*<th className="w-25">Product ID</th>*/}
                  <th className="w-25">Product Name</th>
                  <th className="w-25">Product Price</th>
                </tr>
              </thead>
            </Table>
            <Table>
              <tbody className="justify-content-center">
                {children}
              </tbody>
            </Table>
        </Container> 
)   
}

export default function ViewOrderCard({viewOrderProp}) {

    const {_id, productId, PurchasedOn, email, userId, name, price} = viewOrderProp;

    return (
        <tr>
            <td className="w-25">{PurchasedOn}</td>
            <td className="w-25">{email}</td>
            {/*<td className="w-25">{userId}</td>*/}
            {/*<td className="w-25">{_id}</td>  */}         
            <td className="w-25">{productId}</td>
            <td className="w-25">{name}</td>       
            <td className="w-25">{price}</td>       
        </tr>
    )
}

ViewOrderCard.propTypes = {
    productProp: PropTypes.shape({
        _id: PropTypes.string.isRequired,
        productId: PropTypes.string.isRequired,
        PurchasedOn: PropTypes.string.isRequired
    })
}
