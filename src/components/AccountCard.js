import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import UserContext from '../UserContext';
import Table from 'react-bootstrap/Table'
import React, { useState, useContext } from 'react';


export function AccountCardTable({ children }) {

return (
        <Container>
           <Table striped bordered hover variant="dark">
              <thead>
                <tr>
                </tr>
              </thead>
              <thead>
                <tr>
                  <th className="w-25">First Name</th>
                  <th className="w-25">Last Name</th>
                  <th className="w-25">Email</th>
                  <th className="w-25">Mobile #</th>
                </tr>
              </thead>
            </Table>
            <Table>
              <tbody className="justify-content-center">
                {children}
              </tbody>
            </Table>
        </Container> 
)   
}


export default function UsersCard({usersProp}) {

    const { user } = useContext(UserContext);

	const {_id, firstName, lastName, email, mobileNo} = usersProp;



    return (
            <tr>
              <td className="w-25">{firstName}</td>
              <td className="w-25">{lastName}</td>
              <td className="w-25">{email}</td>
              <td className="w-25">{mobileNo}</td>
            </tr>
    )
}

UsersCard.propTypes = {
    usersProp: PropTypes.shape({
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        email: PropTypes.string.isRequired,
        mobileNo: PropTypes.number.isRequired
    })
}