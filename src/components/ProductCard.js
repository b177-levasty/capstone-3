import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import UserContext from '../UserContext';
import Table from 'react-bootstrap/Table'
import React, { useState, useContext } from 'react';

export function ProductCardTable({ children }) {

const { user } = useContext(UserContext);

return (
    (user.isAdmin !== true) ?

        <Card>
            {children}
        </Card>


    :
        <Container>
           <Table striped bordered hover variant="dark">
              <thead>
                <tr>
                </tr>
              </thead>
              <thead>
                <tr>
                  <th className="w-25">Name</th>
                  <th className="w-25">Description</th>
                  <th className="w-25">Price</th>
                  <th className="w-25">Action</th>
                </tr>
              </thead>
            </Table>
            <Table>
              <tbody className="justify-content-center">
                {children}
              </tbody>
            </Table>
        </Container> 
)   
}

export default function ProductCard({productProp}) {

    const { user } = useContext(UserContext);

	const {_id, name, description, price} = productProp;


    return (
        (user.isAdmin !== true) ?

            // <Card.Body>
            //     <Card.Title>{name}</Card.Title>
            //     <Card.Subtitle>Description:</Card.Subtitle>
            //     <Card.Text>{description}</Card.Text>
            //     <Card.Subtitle>Price:</Card.Subtitle>
            //     <Card.Text>PhP {price}</Card.Text>
            //     <Link className="btn btn-primary" to={`/products/${_id}`}>View Product</Link>    
            // </Card.Body>

            <Card style={{ width: '18rem' }} className="p-3 m-3 justify-context-center">
                <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>PhP {price}</Card.Text>
                    <Link className="btn btn-primary" to={`/products/${_id}`}>View Product</Link>    
                </Card.Body>
            </Card>


        :
        // <Container>
        
        // <Table striped bordered hover variant="dark">
        //   <tbody className="justify-content-center">
        //     <tr>
        //       <td className="w-25">{name}</td>
        //       <td className="w-25">{description}</td>
        //       <td className="w-25">{price}</td>
        //       <td><Link className="btn btn-danger" to={`/products/${_id}`}>View Details</Link></td>
        //     </tr>
        //   </tbody>
        // </Table>
        // </Container>
        <tr>
            <td className="w-25" id="name">{name}</td>
            <td className="w-25" id="description">{description}</td>
            <td className="w-25" id="price">{price}</td> 
            <td><Link className="btn btn-danger" to={`/products/${_id}`}>View Details</Link></td>       
        </tr>

    )
}

ProductCard.propTypes = {
    productProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}