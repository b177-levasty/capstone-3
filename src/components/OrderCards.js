import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Card, Button } from 'react-bootstrap'
import React from 'react';
import { Link } from 'react-router-dom'
import {Container, Row, Col, Form } from 'react-bootstrap'
import Table from 'react-bootstrap/Table'



export function OrderCardTable({ children }) {

return (
        <Container>
           <Table striped bordered hover variant="dark">
              <thead>
                <tr>
                </tr>
              </thead>
              <thead>
                <tr>
                  <th className="w-25">Purchased Date</th>
                  <th className="w-25">Order ID</th>
                  <th className="w-25">Product Name</th>
                </tr>
              </thead>
            </Table>
            <Table>
              <tbody className="justify-content-center">
                {children}
              </tbody>
            </Table>
        </Container> 
)   
}

export default function OrderCard({orderProp}) {

const {_id, name, productId, purchasedOn} = orderProp;

return (
        <tr>
            <td className="w-25">{purchasedOn}</td>
            <td className="w-25">{_id}</td>        
            <td className="w-25">{name}</td>        
        </tr>
)   
}

OrderCard.propTypes = {
orderProp: PropTypes.shape({
    name: PropTypes.string.isRequired,
    _id: PropTypes.string.isRequired,
    productId: PropTypes.string.isRequired,
    PurchasedOn: PropTypes.string.isRequired
})
}

// OrderCard.propTypes = {
//     productProp: PropTypes.shape({
//         _id: PropTypes.string.isRequired,
//         productId: PropTypes.string.isRequired,
//         PurchasedOn: PropTypes.string.isRequired
//     })
// }
