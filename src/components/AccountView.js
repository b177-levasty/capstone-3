import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import Table from 'react-bootstrap/Table'
import UserContext from '../UserContext';


export default function ProductView() {

	const { user } = useContext(UserContext);

	const history = useNavigate();

	const { userId } = useParams();

	const [firsName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");

	// const addToCart = (productId) => {
	// 	fetch(`http://localhost:4000/users/checkout`, {
	// 		method: "POST",
	// 		headers: {
	// 			"Content-Type": "application/json",
	// 			Authorization: `Bearer ${localStorage.getItem('token')}`
	// 		},
	// 		body: JSON.stringify({
	// 			productId: productId
	// 		})
	// 	})
	// 	.then(res => res.json())
	// 	.then(data => {
	// 		console.log(data);

	// 		if(data){
	// 			Swal.fire({
	// 				title: "Successfully enrolled",
	// 				icon: "success",
	// 				text: "Product successfully add to cart."
	// 			})
	// 			history("/products");
	// 		}
	// 		else{
	// 			Swal.fire({
	// 				title: "Something went wrong",
	// 				icon: "error",
	// 				text: "Please try again."
	// 			})
	// 		}
	// 	})
	// }

	// const archive = (userId) => {
	// 	fetch(`http://localhost:4000/products/${productId}/archive`, {
	// 		method: "PUT",
	// 		headers: {
	// 			"Content-Type": "application/json",
	// 			Authorization: `Bearer ${localStorage.getItem('token')}`
	// 		},
	// 		body: JSON.stringify({
	// 			productId: productId
	// 		})
	// 	})
	// 	.then(res => res.json())
	// 	.then(data => {
	// 		console.log(data);

	// 		if(data){
	// 			Swal.fire({
	// 				title: "Successfully enrolled",
	// 				icon: "success",
	// 				text: "Product successfully add to cart."
	// 			})
	// 			history("/products");
	// 		}
	// 		else{
	// 			Swal.fire({
	// 				title: "Something went wrong",
	// 				icon: "error",
	// 				text: "Please try again."
	// 			})
	// 		}
	// 	})
	// }

	const update = (userId) => {
		fetch(`https://protected-fjord-90200.herokuapp.coms/products/${productId}/update`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Successfull",
					icon: "success",
					text: "Product successfully updated."
				})
				history("/products");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(()=> {

		console.log(productId);

		fetch(`https://protected-fjord-90200.herokuapp.com/users/${usersId}`,)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setFirstName(data.firstName);
			setLastName(data.lastName);
			setEmail(data.email);
			setMobileNo(data.mobileNo);
		})

	}, [userId]);


	return(
		(user.isAdmin !== true) ?
		<Container>
			<Row className="mt-3 mb-3 justify-content-center">
				<Card style={{ width: '18rem' }} className="p-3 m-3 justify-context-center">
				  <Card.Body className="text-center">
				    <Card.Title>{name}</Card.Title>
				    <Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
				    <Card.Text>{description}</Card.Text>
				    <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
				    <Card.Text>Php {price}</Card.Text>
				    { user.id !== null ?
				    	<Button variant="primary" onClick={() => addToCart(productId)} block>Add to cart</Button>
				    	:
				    	<Link className="btn btn-danger btn-block" to="/login">Log in to Buy</Link>

				    }
				    
				  </Card.Body>
				</Card>
			</Row>
		</Container>
		:
		// <Container>
		// 	<Row className="mt-3 mb-3 justify-content-center">
		// 		<Card style={{ width: '18rem' }} className="p-3 m-3 justify-context-center">
		// 		  <Card.Body className="text-center">
		// 		    <Card.Title>{name}</Card.Title>
		// 		    <Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
		// 		    <Card.Text>{description}</Card.Text>
		// 		    <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
		// 		    <Card.Text>Php {price}</Card.Text>
		// 		    { user.id !== null ?
		// 		    	<Button variant="primary" onClick={() => archive(productId)} block>Delete</Button>
		// 		    	:
		// 		    	<Link className="btn btn-danger btn-block" to="/login">Log in to Buy</Link>
		// 		    }
		// 		  </Card.Body>
		// 		</Card>
		// 	</Row>
		    <Container>
				<Table striped bordered hover variant="dark">
		          <thead className="justify-content-center">
		            <tr>
		              <th className=" w-25">Name</th>
		              <th className=" w-25">Description</th>
		              <th className=" w-25">Price</th>
		              <th className=" w-25">Options</th>
		            </tr>
		          </thead>
		          <tbody>
		            <tr>
		              <td className="w-25">{name}</td>
		              <td className="w-25">{description}</td>
		              <td className="w-25">{price}</td>
		              <td><Button variant="primary" onClick={() => archive(productId)} block>Delete</Button>
		              </td>
		            </tr>
		          </tbody>
		        </Table>

		    <br/>

		<Form className="position-absolute top-50 start-50 translate-middle bi bi-caret-down-fill text-center" onSubmit={(e) => update(e)}>
		  <h1>Update Product</h1>
		  <Form.Group className="mb-3" controlId="userEmail">

		  	<Form.Label>Product Name:</Form.Label>
		  	<Form.Control
		  		className="text-center"
		  		type="text" 
		  		placeholder="Enter product name" 
		  		value={name}
		  		onChange={e => setName(e.target.value)}
		  		required
		  	/>
		    <Form.Label>Product Description:</Form.Label>
		    <Form.Control 
		    	className="text-center"
		    	type="text" 
		    	placeholder="Enter product description" 
		    	value={description}
		    	onChange={e => setDescription(e.target.value)}
		    	required
		    />
		    <Form.Label>Product Price:</Form.Label>
		    <Form.Control 
		    	className="text-center"
		    	type="number" 
		    	placeholder="Enter products price" 
		    	value={price}
		    	onChange={e => setPrice(e.target.value)}
		    	required
		    />
		    </Form.Group>
		    <Button variant="primary" onClick={() => update(productId)} block>Update</Button>
		</Form>						    	

		    </Container>
	)
}

